/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef ITRAFFICPARTICIPANTMODEL_H
#define ITRAFFICPARTICIPANTMODEL_H

#include <osi_sensorview.pb.h>
#include <osi_trafficcommand.pb.h>
#include <osi_trafficcommandupdate.pb.h>
#include <osi_trafficupdate.pb.h>

#include <chrono>
#include <map>
#include <optional>
#include <string>

namespace osi_traffic_participant
{

/// @brief Return values of a TrafficParticipantUpdate
struct TrafficParticipantUpdateResult
{
  /// \sa https://github.com/OpenSimulationInterface/open-simulation-interface/blob/master/osi_trafficcommandupdate.proto
  osi3::TrafficUpdate traffic_update;
  /// \sa https://github.com/OpenSimulationInterface/open-simulation-interface/blob/master/osi_trafficupdate.proto
  std::optional<osi3::TrafficCommandUpdate> traffic_command_update;
};

class ITrafficParticipantModel
{
public:
  /// @brief Default destructor
  virtual ~ITrafficParticipantModel() = default;

  /// @brief Channel used by the interface user to ask the participant for specific sensor view configuration.
  /// @note Even though the participant can request a maximum cycle time, the interface user can set a smaller one.
  /// @returns a sensor view configuration if the wants to deviate from the default view, otherwise std::nullopt.
  virtual std::optional<osi3::SensorViewConfiguration> GetSensorViewConfigurationRequest() = 0;

  /// @brief Channel used by the interface user to to set/update a sensor view configuration
  /// @param sensor_view_config osi3::SensorViewConfiguration
  virtual void SetSensorViewConfiguration(osi3::SensorViewConfiguration sensor_view_config) = 0;

  /// @brief Channel used by the interface user to update the participant by a step of length delta_time
  /// @param delta_time  Delta time between the step in milliseconds.
  /// @param sensor_view osi3::SensorView containing osi3::GroundTruth containing all the entities
  ///                    (including the controlled entity).
  /// @param traffic_command optional osi3::TrafficCommand for the participant
  virtual TrafficParticipantUpdateResult Update(std::chrono::milliseconds delta_time,
                                                const osi3::SensorView& sensor_view,
                                                const std::optional<osi3::TrafficCommand>& traffic_command) = 0;
};

/// @brief Declares the traffic participant model creation function
/// @param entity_id Unique identifier of the traffic participant
/// @param ground_truth_init Initial global osi3::GroundTruth 
/// @param parameters Map of parameters to be provide to participant
using Create = std::shared_ptr<ITrafficParticipantModel>(uint64_t entity_id,
                                                         const osi3::GroundTruth& ground_truth_init,
                                                         const std::map<std::string, std::string>& parameters);

}  // namespace osi_traffic_participant

#endif  // ITRAFFICPARTICIPANTMODEL_H
